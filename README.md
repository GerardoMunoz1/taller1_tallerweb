# Taller n°1 - Taller de Desarrollo Web

## Acerca
El sitio web consta de un sitio estilo "One-Page" acerca de la gestión de inventarios de negocios. Este proyecto es parte del proyecto en común correspondiente al **grupo 3** del curso, siendo esta la versión desarrollada por Gerardo Muñoz.

## Grupo
- Gerardo Muñoz (versión actual del sitio).
- Benjamin Martin.
- Felipe Carreño.

## Recursos utilizados

Dentro de los recursos utilizados para la creación del sitio están:
- Navegador Brave.
- HTML5.
- CSS.
- Bootstrap.

## Actualizaciones
Se espera seguir progresando en ideas, pudiendo implementar nuevas funcionalidades y novedades en el sitio.
